package com.rahmanabdulrazak1501393.utsmobpro.cinemago.api;

import com.rahmanabdulrazak1501393.utsmobpro.cinemago.model.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class Service {

    @GET("movie/pupular")
    Call<MovieResponse> getPopularMovies(@Query("api_key") String apiKey);

    @GET("movie/top_rated")
    Call<MovieResponse> getTopRatedMovies(@Query("api_key") String apiKey);

}
