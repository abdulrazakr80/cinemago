package com.rahmanabdulrazak1501393.utsmobpro.cinemago.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rahmanabdulrazak1501393.utsmobpro.cinemago.R;
import com.rahmanabdulrazak1501393.utsmobpro.cinemago.model.Movie;

import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder>{
    private Context nContext;
    private List<Movie> movieList;

    public MovieAdapter(Context nContext, List<Movie> movieList){
        this.nContext = nContext;
        this.movieList = movieList;

    }

    @Override
    public MovieAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.movie_card, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MovieAdapter.MyViewHolder viewHolder, int i){
        viewHolder.title();
    }

    @Override
    public int getItemCount(){
        return movieList.size();
    }


    public class MyViewHolder {
    }
}
